package com.test.evoluum.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.test.evoluum.Utils.Utils;
import com.test.evoluum.beans.Ciudad;
import com.test.evoluum.beans.EstadoResponse;
import com.test.evoluum.services.CiudadesServices;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {

	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	@Autowired
	CiudadesServices ciudadesServices;
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/jsonvalue", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value="Obtiene lista en formato json", notes="Obtiene lista de ciudades en formato json" ,nickname="getJson")
	@ResponseBody
	public List<EstadoResponse> getJson(Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}.", locale);
		return ciudadesServices.getEstado();
		
	}
	
	@RequestMapping(value = "/csv", method = RequestMethod.GET, produces = "application/csv")
	@ApiOperation(value="Se obtiene lista de ciudades y retorna en archivo csv", notes="Se obtiene lista de ciudades y retorna en archivo csv" ,nickname="getCsv")
	@ResponseBody
	public void getCsv(Locale locale, Model model,HttpServletRequest request, HttpServletResponse response) throws IOException {
		logger.info("Welcome home! The client locale is {}.", locale);
		String url = request.getSession().getServletContext().getRealPath("/resources/")+"/out.csv";
		
		File file = new File(url);
		PrintWriter writer = null;
		try {
			writer = new PrintWriter(file);
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		StringBuilder sb = new StringBuilder();
		sb = Utils.crateCSVData(ciudadesServices.getEstado());
		
		writer.write(sb.toString());
	    writer.close();
		
	    byte[] bytes = sb.toString().getBytes();
	           
		 response.setContentType("application/csv");
		 response.setHeader("Content-disposition", "inline; filename=out.csv");
		 response.setContentLength(bytes.length);
		 response.getOutputStream().write(bytes);
	     response.getOutputStream().flush();
		 
	}
	
	@RequestMapping(value = "/getciudad/{nom}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value="Se id de la ciudad apartir del nombre", notes="Se id de la ciudad apartir del nombre" ,nickname="getCiudad")
	@ResponseBody
	public String getCiudad(@ApiParam(value = "nom", required = true) @PathVariable(value="nom") String nom, Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}.", locale);
		Ciudad res= ciudadesServices.getCiudadID(nom);
		return res.getId();
	}
	
}
