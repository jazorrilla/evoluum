package com.test.evoluum.Utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import com.test.evoluum.beans.Ciudad;
import com.test.evoluum.beans.Estado;
import com.test.evoluum.beans.EstadoResponse;


public class Utils {

	  public static File crateCSVFile(List<EstadoResponse> data, String path) {
		  
		  File file = new File(path);
		  
		  try {
			PrintWriter writer = new PrintWriter(file);
			StringBuilder sb = new StringBuilder();
		      sb.append("idEstado");
		      sb.append(";");
		      sb.append("siglaEstado");
		      sb.append(";");
		      sb.append("regiaoNome");
		      sb.append(";");
		      sb.append("nomeCidade");
		      sb.append(";");
		      sb.append("nomeMesorregiao");
		      sb.append(";");
		      sb.append("nomeFormatado");
		      sb.append('\n');

		      for(EstadoResponse d : data) {
			      sb.append(d.getIdEstado());
			      sb.append(";");
			      sb.append(d.getSiglaEstado());
			      sb.append(";");
			      sb.append(d.getRegiaoNome());
			      sb.append(";");
			      sb.append(d.getNomeCidade());
			      sb.append(";");
			      sb.append(d.getNomeMesorregiao());
			      sb.append(";");
			      sb.append(d.getNomeFormatado());
			      sb.append('\n');
		      }
		      
		      writer.write(sb.toString());
		      writer.close();
		      System.out.println("done!");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return file;
		  
	}
	
	  public static StringBuilder crateCSVData(List<EstadoResponse> data) {
		  
			StringBuilder sb = new StringBuilder();
		      sb.append("idEstado");
		      sb.append(";");
		      sb.append("siglaEstado");
		      sb.append(";");
		      sb.append("regiaoNome");
		      sb.append(";");
		      sb.append("nomeCidade");
		      sb.append(";");
		      sb.append("nomeMesorregiao");
		      sb.append(";");
		      sb.append("nomeFormatado");
		      sb.append('\n');

		      for(EstadoResponse d : data) {
			      sb.append(d.getIdEstado());
			      sb.append(";");
			      sb.append(d.getSiglaEstado());
			      sb.append(";");
			      sb.append(d.getRegiaoNome());
			      sb.append(";");
			      sb.append(d.getNomeCidade());
			      sb.append(";");
			      sb.append(d.getNomeMesorregiao());
			      sb.append(";");
			      sb.append(d.getNomeFormatado());
			      sb.append('\n');
		      }
		      
		      
		return sb;
		  
	}  
	  
	  
	 public static String getCiudadId(List<Ciudad> list, String nom){
		 String result="";
		 for(Ciudad d : list) {
		      if(d.getNome().equalsIgnoreCase(nom)) {
		    	  result = d.getId();
		    	  break;
		      }
	      }
		 return result;
	 }
}
