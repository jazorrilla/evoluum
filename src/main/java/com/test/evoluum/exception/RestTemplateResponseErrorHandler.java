package com.test.evoluum.exception;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import org.apache.commons.io.IOUtils;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.RestClientException;

import org.slf4j.Logger;
@Component
public class RestTemplateResponseErrorHandler extends DefaultResponseErrorHandler{
	private static final Logger logger = LoggerFactory.getLogger(RestTemplateResponseErrorHandler.class);
	@Override
	  public void handleError(ClientHttpResponse response) throws IOException {
		String jsonError = ""+IOUtils.toString(response.getBody(), StandardCharsets.UTF_8.name());
		logger.error("ERROR al consumir servicio externo ===  Codigo: "+response.getStatusCode() +", Status : "+response.getStatusText()+" , Mensaje: "+jsonError);
		HttpStatus statusCode = response.getStatusCode();
		throw new RestClientException("Ocurrio un error inesperado, codigo : [" + statusCode + "]");
	  }
	
}
