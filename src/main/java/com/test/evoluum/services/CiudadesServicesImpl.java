package com.test.evoluum.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.test.evoluum.beans.Ciudad;
import com.test.evoluum.beans.ContenedorEstados;
import com.test.evoluum.beans.Estado;
import com.test.evoluum.beans.EstadoCiudad;
import com.test.evoluum.beans.EstadoResponse;
import com.test.evoluum.exception.RestTemplateResponseErrorHandler;

@Service
public class CiudadesServicesImpl implements CiudadesServices{
	Logger logger = LoggerFactory.getLogger(CiudadesServicesImpl.class);
	@Override
	public List<EstadoResponse> getEstado() {
		Gson gson = new Gson();
		Properties props = System.getProperties();
	    props.setProperty("https.protocols", "TLSv1,TLSv1.1,TLSv1.2");
		String uri=String.format("https://servicodados.ibge.gov.br/api/v1/localidades/estados");
		
		RestTemplate restTemplate = new RestTemplate();
	    restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter());
	    
	    /* HEADER */
	    HttpHeaders headers = new HttpHeaders();
	    headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
	    headers.setContentType(MediaType.APPLICATION_JSON);
	    headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
	    
	    HttpEntity<?> request = new HttpEntity<String>(headers);
		
	    /* CALL */
	    ((SimpleClientHttpRequestFactory)restTemplate.getRequestFactory()).setConnectTimeout(Integer.parseInt("50000"));
		((SimpleClientHttpRequestFactory)restTemplate.getRequestFactory()).setReadTimeout(Integer.parseInt("50000"));
		
		restTemplate.setErrorHandler(new RestTemplateResponseErrorHandler());
		
		logger.info("Vamos a consumir WS - URL: "+uri);
		List<Estado> respuesta = new ArrayList<Estado>();
		try {
			ResponseEntity<String> rst = restTemplate.exchange(uri, HttpMethod.GET, request, String.class);
			logger.info("Respuesta de la llamada al WS - objeto = : "+rst.getBody());
			
			respuesta = stringToArray(rst.getBody(), Estado[].class);
			
			List<EstadoCiudad> resp = new ArrayList<EstadoCiudad>();
			for(Estado e : respuesta) {
					EstadoCiudad estadoCiudad = new EstadoCiudad();
					estadoCiudad.setEstado(e);
					estadoCiudad.setCiudades(getCiudad(e.getId()));
					resp.add(estadoCiudad);
			}
			
			List<EstadoResponse> processResp = new ArrayList<EstadoResponse>();
			for (EstadoCiudad ec : resp) {
				for(Ciudad c : ec.getCiudades()) {
					processResp.add(new EstadoResponse(ec.getEstado(),c));
				}
			}
			
			return processResp;
		}catch (RestClientException e) {
			throw new RestClientException(e.getMessage());
		}
	}
	@Override
	public List<Ciudad> getCiudad(String idEstado) {
		Gson gson = new Gson();
		Properties props = System.getProperties();
	    props.setProperty("https.protocols", "TLSv1,TLSv1.1,TLSv1.2");
		String uri = null;
		if(idEstado != null && !"".equals(idEstado)) {
			uri=String.format("https://servicodados.ibge.gov.br/api/v1/localidades/estados/"+idEstado+"/municipios");
		}else {
			uri=String.format("https://servicodados.ibge.gov.br/api/v1/localidades/municipios");
		}
		
		RestTemplate restTemplate = new RestTemplate();
	    restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter());
	    
	    /* HEADER */
	    HttpHeaders headers = new HttpHeaders();
	    headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
	    headers.setContentType(MediaType.APPLICATION_JSON);
	    headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
	    
	    HttpEntity<?> request = new HttpEntity<String>(headers);
		
	    /* CALL */
	    ((SimpleClientHttpRequestFactory)restTemplate.getRequestFactory()).setConnectTimeout(Integer.parseInt("50000"));
		((SimpleClientHttpRequestFactory)restTemplate.getRequestFactory()).setReadTimeout(Integer.parseInt("50000"));
		
		restTemplate.setErrorHandler(new RestTemplateResponseErrorHandler());
		
		logger.info("Vamos a llamar al WS - URL: "+uri);
		List<Ciudad> respuesta = new ArrayList<Ciudad>();
		ContenedorEstados contenedorEstados = new ContenedorEstados();
		try {
			ResponseEntity<String> rst = restTemplate.exchange(uri, HttpMethod.GET, request, String.class);
			logger.info("Respuesta de la llamada al WS - objeto = : "+rst.getBody());
			respuesta = stringToArray(rst.getBody(), Ciudad[].class);
			return respuesta;
		}catch (RestClientException e) {
			throw new RestClientException(e.getMessage());
		}
	}
	
	@Override
	@Cacheable(value="ciudad", key="#nom")
	public Ciudad getCiudadID(String nom) {
		logger.info("No esta en cache, se llama al WS");
		List<Ciudad> listCiu = getCiudad(null);
		Ciudad res = new Ciudad();		
		for(Ciudad d : listCiu) {
		      if(d.getNome().equalsIgnoreCase(nom)) {
		    	  res = d;
		    	  break;
		      }
	      }
		return res;
	}
	
	public static <T> List<T> stringToArray(String s, Class<T[]> clazz) {
	    T[] arr = new Gson().fromJson(s, clazz);
	    return Arrays.asList(arr); 
	}

}
