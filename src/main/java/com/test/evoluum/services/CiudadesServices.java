package com.test.evoluum.services;

import java.util.List;

import com.test.evoluum.beans.Ciudad;
import com.test.evoluum.beans.EstadoResponse;

public interface CiudadesServices {
	public List<EstadoResponse> getEstado();
	public List<Ciudad> getCiudad(String idEstado);
	public Ciudad getCiudadID(String nom);
}
