package com.test.evoluum.beans;

import java.util.ArrayList;
import java.util.List;

public class EstadoCiudad {
	private Estado estado;
	private List<Ciudad> ciudades;
	public Estado getEstado() {
		return estado;
	}
	public void setEstado(Estado estado) {
		this.estado = estado;
	}
	public List<Ciudad> getCiudades() {
		return ciudades;
	}
	public void setCiudades(List<Ciudad> ciudades) {
		this.ciudades = ciudades;
	}
	public EstadoCiudad() {
		super();
		this.estado = new Estado();
		this.ciudades = new ArrayList<Ciudad>();
	}
}
