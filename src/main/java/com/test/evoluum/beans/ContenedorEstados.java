package com.test.evoluum.beans;

import java.util.List;

public class ContenedorEstados {
	private List<Estado> estados;

	public List<Estado> getEstados() {
		return estados;
	}

	public void setEstados(List<Estado> estados) {
		this.estados = estados;
	}

	@Override
	public String toString() {
		return "ContenedorEstados [estados=" + estados + "]";
	}
	
	
}
