package com.test.evoluum.beans;

public class Ciudad {

	public class Microregion {
	    public String id;
	    public String nome;
	    public Mesorregiao mesorregiao;
	}
	
	public class Mesorregiao{
		public String id;
		public String nome;
		public UF UF;
    }
	
	public class UF{
		public String id;
		public String sigla;
		public String nome;
		public Region regiao;
   }
	
	public String id;
	public String nome;
	public Microregion microrregiao;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Microregion getMicrorregiao() {
		return microrregiao;
	}
	public void setMicrorregiao(Microregion microrregiao) {
		this.microrregiao = microrregiao;
	}
		
}
