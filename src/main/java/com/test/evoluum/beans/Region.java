package com.test.evoluum.beans;

public class Region {
	public String id;
	public String sigla;
	public String nome;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getSigla() {
		return sigla;
	}
	public void setSigla(String sigla) {
		this.sigla = sigla;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public String toString() {
		return "Region [id=" + id + ", sigla=" + sigla + ", nome=" + nome + "]";
	}
	
}
