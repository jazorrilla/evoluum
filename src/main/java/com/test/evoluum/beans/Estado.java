package com.test.evoluum.beans;

public class Estado {
	
	private String id;
	private String sigla;
	private String nome;
	private Region regiao;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getSigla() {
		return sigla;
	}
	public void setSigla(String sigla) {
		this.sigla = sigla;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Region getRegiao() {
		return regiao;
	}
	public void setRegiao(Region regiao) {
		this.regiao = regiao;
	}
	@Override
	public String toString() {
		return "Estado [id=" + id + ", sigla=" + sigla + ", nome=" + nome + ", regiao=" + regiao + "]";
	}
	

}
