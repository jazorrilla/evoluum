package com.test.evoluum.beans;

public class EstadoResponse {
	private String idEstado;
	private String siglaEstado;
	private String regiaoNome;
	private String nomeCidade;
	private String nomeMesorregiao;
	private String nomeFormatado;
	
	public String getIdEstado() {
		return idEstado;
	}
	public void setIdEstado(String idEstado) {
		this.idEstado = idEstado;
	}
	public String getSiglaEstado() {
		return siglaEstado;
	}
	public void setSiglaEstado(String siglaEstado) {
		this.siglaEstado = siglaEstado;
	}
	public String getRegiaoNome() {
		return regiaoNome;
	}
	public void setRegiaoNome(String regiaoNome) {
		this.regiaoNome = regiaoNome;
	}
	public String getNomeCidade() {
		return nomeCidade;
	}
	public void setNomeCidade(String nomeCidade) {
		this.nomeCidade = nomeCidade;
	}
	public String getNomeMesorregiao() {
		return nomeMesorregiao;
	}
	public void setNomeMesorregiao(String nomeMesorregiao) {
		this.nomeMesorregiao = nomeMesorregiao;
	}
	public String getNomeFormatado() {
		return nomeFormatado;
	}
	public void setNomeFormatado(String nomeFormatado) {
		this.nomeFormatado = nomeFormatado;
	}
	public EstadoResponse() {
		super();
	}
	public EstadoResponse(Estado e, Ciudad c) {
		super();
		this.setIdEstado(e.getId());
		this.setNomeCidade(c.getNome());
		this.setNomeFormatado(c.getNome()+"/"+c.getMicrorregiao().mesorregiao.UF.sigla);
		this.setNomeMesorregiao(c.getMicrorregiao().mesorregiao.nome);
		this.setRegiaoNome(c.getMicrorregiao().mesorregiao.UF.regiao.getNome());
		this.setSiglaEstado(e.getSigla());
	}
	
}
